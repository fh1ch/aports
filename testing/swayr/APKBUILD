# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=swayr
pkgver=0.7.0
pkgrel=0
pkgdesc="A window switcher (and more) for Sway"
url="https://sr.ht/~tsdh/swayr/"
arch="aarch64 armhf armv7 ppc64le x86 x86_64"  # limited by rust/cargo
license="GPL-3.0-or-later"
makedepends="cargo"
source="$pkgname-$pkgver.tar.gz::https://git.sr.ht/~tsdh/swayr/archive/v$pkgver.tar.gz"
builddir="$srcdir/$pkgname-v$pkgver"

prepare() {
	default_prepare

	# Optimize binary for size.
	cat >> Cargo.toml <<-EOF

		[profile.release]
		codegen-units = 1
		lto = true
		opt-level = "z"
		panic = "abort"
	EOF

	cargo fetch --locked
}

build() {
	cargo build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	cargo install --locked --offline --path . --root="$pkgdir/usr"
	rm "$pkgdir"/usr/.crates*
}

sha512sums="
505238327f7af2340641380a454537e42d21b32628e07de5f2268bd203a3b32d039338b5f5c94edcc5c8fd111d625aac4fec00251870d0d9f56f5fbe34bd367a  swayr-0.7.0.tar.gz
"
